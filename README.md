# README #

This Django project is a demonstration that I recetly did for a company while interviewing for a development position. I've done my best to censor their company name so that I can exhibit this project to others. The requirements are specified at:

requirements.txt

Below are two descriptions of the project; a short one and a long one. Please read both if you have time. Before those a brief description of my approach to this problem.

### Approach ###

At first, I thought that using AngularJS would be a good fit for this problem. Being a client-side MVC framework, and that the site should be a single-page app, I thought AngularJS would be appropriate. However, that required me to first construct a RESTful web service to serve the given database. I turned to Django for my server-side work, and found that it was much faster to simply rely on its powerful ORM querying model, and a small bit of client-side code.

### Short Version ###

* To run the project, install python, then install Django 1.10.6 and start server as usual (./manage.py runserver)
* visit 'localhost:8000' in your browser'
* The site should meet all the requirements (even extras) and should look like the site shown in the screenshot below.

### Long Version ###

This section will suppliment the info mentioned above.

* In addition to the requirements above, the site also needs an internet connection, as jQuery and bootstrap are loaded via CDNs. 
* The most relevant files are in the 'census' folder. They are: views.py, static/census.css, static/census.js, and templates/census/index.html. 'census/models.py' is crucial, but is auto-generated and untouched.
* The database was used as-is and was not modified. This was a bit heart-breaking as the data seems very dirty. There are many records will no information (all fields NULL), and some of the fields seem improperly named (e.g. mace, hispanice). Even the though the database was used as-is, some of the records were filtered out during usage. Those were any records where the field that the user-selected for viewing was NULL. The reason I did this was that, at a first glance, these records seemed to not offer any other information (all fields were NULL), but this is an assumption I've made and did not fully analyze.
* I could have simplified the project greatly by putting scripts inline and collapsing the Django 'apps' into one main one. I decided not to because I felt it would demonstrate an improper attitude toward the project. So the site has its own app, and all scripts, etc, are organized well enough. 
* I assumed that user security was not under test here. So I did not implement any log-in functionality.
* I also assumed that project "health" was not under test, so I did not create any unit or integration tests here.

### Screenshot of Site ###

![Screenshot](screenshot.png)
