from django.shortcuts import render
from django.http import JsonResponse, HttpResponseBadRequest
from census.models import CensusLearnSql
from django.db.models import Sum, Avg, Count, F
import json

MAX_RESULTS_MAX = 100
MAX_RESULTS_MIN = 1

def index(request):
    fields = CensusLearnSql._meta.get_fields();

    #create simple list of field names
    fields_list = []
    for field in fields:
      name = field.name
      if name != 'id':
        fields_list.append(name)
         
    return render(request, 'census/index.html', { 'fields' : fields_list })

def value_count(request):
    selected_column = request.GET.get('selected_column')
    selected_max_results_raw = request.GET.get('selected_max_results')

    #check that inputs are not None
    if selected_column == None or selected_max_results_raw == None:
        return HttpResponseBadRequest()
    
    #check that max_results is an int, and cast
    try:
        selected_max_results = int(selected_max_results_raw)
    except ValueError:
        return HttpResponseBadRequest()


    #check that max_results is within limits
    if not MAX_RESULTS_MIN <= selected_max_results <= MAX_RESULTS_MAX:
        return HttpResponseBadRequest() 

    #check that given field actuallys exists
    fields = CensusLearnSql._meta.get_fields();
    field_exists = False
    if selected_column == 'id':
        return HttpResponseBadRequest()
    else:
        for field in fields:
            if field.name == selected_column:
                field_exists = True
                break
    if not field_exists:
        return HttpResponseBadRequest()
    
    #create main query
    results = CensusLearnSql.objects \
                  .annotate(selected_column=F(selected_column)) \
                  .values('selected_column') \
                  .exclude(selected_column=None) \
                  .distinct() \
                  .annotate( \
                      value_count=Count(selected_column), \
                      avg_age=Avg('age')) \
                  .order_by('-value_count')
 
    #derive values_clipped (aggregated) and rows_clipped
    results_count = results.count()
    if results_count > selected_max_results:
        values_clipped = results_count - selected_max_results
    else:
        values_clipped = 0    

    #apply selected max results
    results = results[:selected_max_results]

    #determine rows clipped
    rows_represented = results.aggregate( \
                           shown_results_sum=Sum('value_count')) \
                           ['shown_results_sum']

    total_rows = CensusLearnSql.objects \
                     .annotate(selected_column=F(selected_column)) \
                     .values(selected_column) \
                     .exclude(selected_column=None) \
                     .count()

    rows_clipped = total_rows - rows_represented

    results_as_json = list(results)
    return JsonResponse( { 'results' : results_as_json, 'values_clipped' : values_clipped, 'rows_clipped' : rows_clipped }, safe=False)

