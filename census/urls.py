from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^census/value_count/$', views.value_count, name='value_count'),
]
