var NO_COLUMN_SELECTED = 'unselected';
var AGE_DECIMAL_PLACES = 2;

//initialize column select element
$("#column_select").val(NO_COLUMN_SELECTED);


// perform an ajax query for census data
function query() {
    var selected_column = $("#column_select").val();
    var selected_max_results = $("#max_results_select").val();

    //perform query
    var request = $.ajax({
        method: "GET",
        url: "/census/value_count",
        data: {
            selected_column: selected_column,
            selected_max_results: selected_max_results
        },
        dataType: "json"
    });

    // handle success
    request.done(function(response_data) {
        var results = response_data['results'];
        var values_clipped = response_data['values_clipped'];
        var rows_clipped = response_data['rows_clipped'];

        //populate clipped values summary
        $("#values_clipped").text(values_clipped);
        $("#rows_clipped").text(rows_clipped);

        //clear results table
        $("#results_table tbody").empty();

        //for each result, add table row
        $.each(results, function(i, item) {
            var rounded_age = item.avg_age.toFixed(AGE_DECIMAL_PLACES);
            var $tr = $('<tr>').append(
                $('<td>').text(item.selected_column),
                $('<td>').text(item.value_count),
                $('<td>').text(rounded_age)
            );
            $("#results_table tbody").append($tr);
        });

        //show (unhide) results and clipped values summary 
        show_results_and_hide_loader();
    });

    //handle failure
    request.fail(function(jqXHR, textStatus) {
        alert("There was an error. Email Marco.");
    });
}

// returns true is input is valid and query can be made
function can_query() {
    var selected_column = $("#column_select").val();
    return selected_column !== NO_COLUMN_SELECTED;
}

//handle changes to form input
function input_changed() {
    if (can_query()) {
        hide_results_and_show_loader();
        query();
    }
}

//hide results table and summary info
function hide_results_and_show_loader(){
    $("#results_table").hide();
    $("#clipped_summary_row").hide();
    $("#loader").show();
    
}

//show results table and summary info
function show_results_and_hide_loader(){
    $("#results_table").show();
    $("#clipped_summary_row").show();
    $("#loader").hide();
}
